'use strict';

var started = false;
var counter = 0;

function clockHandler() {
  if (started) {
    counter += 500;
  }
  var diff = new Date(counter);

  $("#timer2").html(("0" + diff.getUTCMinutes()).slice(-2) + ":" + ("0" + diff.getUTCSeconds()).slice(-2));
}

$(document).keyup(function(e){
  console.log(e.keyCode)
  if (e.keyCode == 83) { // key s
    if (!started) {
      started = true;
    } else {
      started = false;
    }
  }
  if (e.keyCode == 68) { // key d
    counter = 0;
  }
});

function start() {
  window.setInterval(clockHandler, 500)
}
