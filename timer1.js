'use strict';

var started = false;
var begining = new Date()

function clockHandler() {
  if (started) {
    var now = new Date();
    var diff = new Date(now - begining);
  } else {
    var diff = new Date(0)
  }

  if (diff.getUTCDate() == 1) {
    $("#timer1").html(("0" + diff.getUTCHours()).slice(-2) + ":" + ("0" + diff.getUTCMinutes()).slice(-2) + ":" + ("0" + diff.getUTCSeconds()).slice(-2));
  } else {
    $("#timer1").html("24:00:00");
  }

}

$(document).keyup(function(e){
  console.log(e.keyCode)
  if (e.keyCode == 32) { // key space
    if (!started) {
      started = true;
      begining = Date.now();
      window.location.hash = begining
    }
  }
});

function start() {
  window.setInterval(clockHandler, 500)

  if (window.location.hash.length > 0) {
    begining = parseInt(window.location.hash.replace('#', ''))
    started = true;
  }
}
