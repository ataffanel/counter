

var STEP_SIZE = 50;

var currentStep = -1;
var currentTime = 0;

var paused = false;

function stepHandler() {
  var nextStep = currentStep + 1;

  if (paused) {
    currentTime = 0;
  } else if (nextStep < sequence.length && currentTime >= sequence[nextStep]["time"]) {
    if (currentStep >= 0) {
      if (sequence[currentStep]["class"] != undefined) {
        $("#counter").removeClass(sequence[currentStep]["class"])
      }
    }

    if (sequence[nextStep]["type"] == "pause") {
      paused = true;
      currentTime = 0;
    } else if (sequence[nextStep]["type"] == "text") {
      $("#counter").html(sequence[nextStep]["text"])

      if (sequence[nextStep]["class"] != undefined) {
        $("#counter").addClass(sequence[nextStep]["class"])
      }
    } else {
      console.error("Bad type: '" + sequence[nextStep]["type"] + "'")
    }

    currentStep = nextStep;
  }

  currentTime += STEP_SIZE / 1000;
}

$(document).keyup(function(e){
  console.log(e.keyCode)
  if (e.keyCode == 32 || e.keyCode == 83) { // Espace ou S
    paused = false;
  }
});

function start() {
  window.setInterval(stepHandler, STEP_SIZE)
}
